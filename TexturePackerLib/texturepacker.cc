#include <cassert>
#include <algorithm>
#include "texturepacker.h"

//---------------------------------------------------------------------//
//                         Texture Packer                              //
//---------------------------------------------------------------------//
TexturePacker::TexturePacker( )
	: m_Images(),  m_pRoot(NULL), m_FinalImage(0), m_FinalImageWidth(0), m_FinalImageHeight(0), m_FinalImageBytesPerPixel(0)
{
	ilInit( ); // initialize DevIL
	ilGenImages( 1, &m_FinalImage );
	assert( m_FinalImage > 0 );
}
//---------------------------------------------------------------------//
TexturePacker::~TexturePacker( )
{
	clear( );
	ilDeleteImage( m_FinalImage );
	ilShutDown( ); // deinitialize DevIL
}
//---------------------------------------------------------------------//
int TexturePacker::add( const tchar *imageFilename )
{
	ILuint imageId = 0;
		
	ilGenImages( 1, &imageId );
	ilBindImage( imageId );
	assert( imageId > 0 );
	
	if( !ilLoadImage( imageFilename ) )
	{
		ILenum errorCode = ilGetError( );
		// TODO: Report errors.
		ilDeleteImage( imageId );
		return -1;
	}

	Image image;
	image.imageId    = imageId;
	image.x          = 0;
	image.y          = 0;
	image.width      = ilGetInteger( IL_IMAGE_WIDTH );
	image.height     = ilGetInteger( IL_IMAGE_HEIGHT );

	m_Images.push_back( image );

	return m_Images.size( ) - 1;
}
//---------------------------------------------------------------------//
void TexturePacker::clear( )
{
	for( uint i = 0; i < m_Images.size( ); i++ )
	{
		ilDeleteImage( m_Images[ i ].imageId );
	}
	m_Images.clear( );
}
//---------------------------------------------------------------------//
bool TexturePacker::texture( uint width, uint height, ushort bytesPerPixel )
{
	sortImages( );
	freeTree( m_pRoot );

	// Build a tree to utilize all of the space.
	m_pRoot = new Rect( 0, 0, width, height );
	for( uint i = 0; i < m_Images.size( ); i++ )
	{
		bool isRectAssigned = insertImage( m_pRoot, &m_Images[ i ] );

		if( !isRectAssigned ) 
		{
			freeTree( m_pRoot );
			return false;
		}
	}

	// Use the tree to generate the final texture.
	m_FinalImageWidth         = width;
	m_FinalImageHeight        = height;
	m_FinalImageBytesPerPixel = bytesPerPixel;
	ilBindImage( m_FinalImage );
	ILenum format = bytesPerPixel == 3 ? IL_RGB : IL_RGBA;
	ilTexImage( m_FinalImageWidth, m_FinalImageHeight, 1, m_FinalImageBytesPerPixel, format, IL_UNSIGNED_BYTE, NULL );
	memset( ilGetData( ), 0xFF, bytesPerPixel * width * height );

	if( !blitTree( m_pRoot ) )
	{
		freeTree( m_pRoot );
		return false;
	}

	freeTree( m_pRoot );
	return true;
}
//---------------------------------------------------------------------//
byte *TexturePacker::pixels( )
{
	ilBindImage( m_FinalImage );
	return ilGetData( );
}
//---------------------------------------------------------------------//
bool TexturePacker::save( const tchar *filename )
{
	ilBindImage( m_FinalImage );
	if( !ilSaveImage( filename ) ) return false;

	return true;
}
//---------------------------------------------------------------------//
const TexturePacker::ImageCollection &TexturePacker::images( ) const
{
	return m_Images;
}
//---------------------------------------------------------------------//
bool TexturePacker::blitTree( Rect *pTree, bool blend )
{
	if( pTree == NULL || pTree->pImage == NULL ) return true;

	if( !blitTree( pTree->pChildren[ LEFT ] ) )  return false;
	if( !blitTree( pTree->pChildren[ RIGHT ] ) ) return false;

	if( blend )
	{
		ilEnable( IL_BLIT_BLEND );
	}
	else
	{
		ilDisable( IL_BLIT_BLEND );
	}

	if( !ilBlit( pTree->pImage->imageId, pTree->x, pTree->y, 0, 0, 0, 0, pTree->pImage->width, pTree->pImage->height, 1 ) )
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------//
void TexturePacker::freeTree( Rect *&pTree )
{
	if( pTree == NULL ) return;

	freeTree( pTree->pChildren[ LEFT ] );
	freeTree( pTree->pChildren[ RIGHT ] );

	delete pTree;
	pTree = NULL;
}
//---------------------------------------------------------------------//
bool TexturePacker::insertImage( Rect *&pTree, Image *pImage )
{
	if( pTree->isContainedWithin( pImage ) )
	{
		if( pTree->isAssigned( ) )
		{
			if( insertImage( pTree->pChildren[ LEFT ], pImage ) )
			{
				return true;
			}
			else
			{
				return insertImage( pTree->pChildren[ RIGHT ], pImage );
			}

		}
		else
		{
			pTree->assignImage( pImage );
			return true;
		}
	}
	
	return false;
}
//---------------------------------------------------------------------//
void TexturePacker::sortImages( )
{
	if( m_Images.size( ) > 1 )
	{
		std::sort( m_Images.begin(), m_Images.end(), imageAreaComparer );
		//assert( imageAreaComparer(m_Images[0], m_Images[1]) );
	}	
}
//---------------------------------------------------------------------//
bool TexturePacker::imageAreaComparer( const Image &left, const Image &right )
{
	return left.width * left.height > right.width * right.height;
}
//---------------------------------------------------------------------//
//                              Rect                                   //
//---------------------------------------------------------------------//
bool TexturePacker::Rect::isAssigned( ) const
{
	return pImage != NULL;
}
//---------------------------------------------------------------------//
bool TexturePacker::Rect::isContainedWithin( const Image *pImage ) const
{
	assert( pImage != NULL );
	return pImage->width <= width && pImage->height <= height;
}
//---------------------------------------------------------------------//
void TexturePacker::Rect::assignImage( Image *pImage )
{
	assert( pImage != NULL );

	// Form a sub-rectangle along the longest edge.
	if( pImage->width < pImage->height ) // form rect along height-side
	{
		pChildren[ LEFT ]  = new Rect( x + pImage->width, y, width - pImage->width, pImage->height ); 
		pChildren[ RIGHT ] = new Rect( x, y + pImage->height, width, height - pImage->height ); 
	}
	else // imageWidth >= imageHeight
	{
		pChildren[ LEFT ]  = new Rect( x, y + pImage->height, pImage->width, height - pImage->height ); 
		pChildren[ RIGHT ] = new Rect( x + pImage->width, y, width - pImage->width, height ); 
	}

	pImage->x    = x;
	pImage->y    = y;
	this->pImage = pImage;
}
//---------------------------------------------------------------------//
//                              Image                                  //
//---------------------------------------------------------------------//
byte *TexturePacker::Image::pixels( )
{
	ilBindImage( imageId );
	return ilGetData( );
}
//---------------------------------------------------------------------//
