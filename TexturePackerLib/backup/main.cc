#include <iostream>
#include <cassert>
#include "texturepacker.h"

using namespace std;

int main( int argc, char *argv[] )
{
	TexturePacker packer;

#if 1
	packer.add( "test1/link_running_right_1.tga" );
	packer.add( "test1/link_running_right_2.tga" );
	packer.add( "test1/link_running_right_3.tga" );
	packer.add( "test1/link_running_right_4.tga" );
	packer.add( "test1/link_running_right_5.tga" );
	packer.add( "test1/link_running_right_6.tga" );
	packer.add( "test1/link_running_right_7.tga" );
	packer.add( "test1/link_running_right_8.tga" );
	bool bTextureCreated = packer.texture( 64, 128 );
	assert( bTextureCreated );
	bool bFileSaved = packer.save( "link_out.tga" );
	assert( bFileSaved );
#else
	packer.add( "C:/projects/TexturePacker/test2/095.jpg" );
	packer.add( "C:/projects/TexturePacker/test2/dc.jpg" );
	packer.add( "C:/projects/TexturePacker/test2/joe.jpg" );
	packer.add( "C:/projects/TexturePacker/test2/oregon.jpg" );
	packer.add( "C:/projects/TexturePacker/test2/red.bmp" );
	bool bTextureCreated = packer.texture( 400, 400 );
	assert( bTextureCreated );
	bool bFileSaved = packer.save( "out.bmp" );
	assert( bFileSaved );
#endif


	return 0;
}
