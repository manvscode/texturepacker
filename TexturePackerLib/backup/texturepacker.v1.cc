#include <cassert>
#include <algorithm>
#include "texturepacker.h"

//---------------------------------------------------------------------//
//                         Texture Packer                              //
//---------------------------------------------------------------------//
TexturePacker::TexturePacker( )
	: m_Images(),  m_pRoot(NULL), m_FinalImage(0), m_FinalImageWidth(0), m_FinalImageHeight(0), m_FinalImageBytesPerPixel(0)
{
	ilInit( ); // initialize DevIL
	ilGenImages( 1, &m_FinalImage );
	assert( m_FinalImage > 0 );
}
//---------------------------------------------------------------------//
TexturePacker::~TexturePacker( )
{
	clear( );
	ilDeleteImage( m_FinalImage );
	ilShutDown( ); // deinitialize DevIL
}
//---------------------------------------------------------------------//
bool TexturePacker::add( const tchar *imageFilename )
{
	Image imageId = 0;
		
	ilGenImages( 1, &imageId );
	ilBindImage( imageId );
	assert( imageId > 0 );
	
	if( !ilLoadImage( imageFilename ) )
	{
		ILenum errorCode = ilGetError( );
		// TODO: Report errors.
		ilDeleteImage( imageId );
		return false;
	}

	m_Images.push_back( imageId );

	return true;
}
//---------------------------------------------------------------------//
void TexturePacker::clear( )
{
	ilDeleteImages( m_Images.size( ), &m_Images[ 0 ] );
	m_Images.clear( );
}
//---------------------------------------------------------------------//
bool TexturePacker::texture( uint width, uint height, ushort bytesPerPixel )
{
	sortImages( );
	freeTree( m_pRoot );

	// Build a tree to utilize all of the space.
	m_pRoot = new Rect( 0, 0, width, height );
	for( uint i = 0; i < m_Images.size( ); i++ )
	{
		bool isRectAssigned = insertImage( m_pRoot, &m_Images[ i ] );

		if( !isRectAssigned ) 
		{
			freeTree( m_pRoot );
			return false;
		}
	}

	// Use the tree to generate the final texture.
	m_FinalImageWidth         = width;
	m_FinalImageHeight        = height;
	m_FinalImageBytesPerPixel = bytesPerPixel;
	ilBindImage( m_FinalImage );
	ILenum format = bytesPerPixel == 3 ? IL_RGB : IL_RGBA;
	ilTexImage( width, height, 1, bytesPerPixel, format, IL_UNSIGNED_BYTE, NULL );
	memset( ilGetData( ), 0xFF, bytesPerPixel * width * height );

	if( !blitTree( m_pRoot ) )
	{
		freeTree( m_pRoot );
		return false;
	}

	freeTree( m_pRoot );
	return true;
}
//---------------------------------------------------------------------//
byte *TexturePacker::pixels( )
{
	ilBindImage( m_FinalImage );
	return ilGetData( );
}
//---------------------------------------------------------------------//
bool TexturePacker::save( const char *filename )
{
	ilBindImage( m_FinalImage );
	return ilSaveImage( filename );
}
//---------------------------------------------------------------------//
bool TexturePacker::blitTree( Rect *pTree, bool blend )
{
	if( pTree == NULL || pTree->pImage == NULL ) return true;

	if( !blitTree( pTree->pChildren[ LEFT ] ) )  return false;
	if( !blitTree( pTree->pChildren[ RIGHT ] ) ) return false;

	ilBindImage( *pTree->pImage );
	uint imageWidth  = ilGetInteger( IL_IMAGE_WIDTH );
	uint imageHeight = ilGetInteger( IL_IMAGE_HEIGHT );
	
	ilBindImage( m_FinalImage );

	if( blend )
	{
		ilEnable( IL_BLIT_BLEND );
	}
	else
	{
		ilDisable( IL_BLIT_BLEND );
	}

	return ilBlit( *pTree->pImage, pTree->x, pTree->y, 0, 0, 0, 0, imageWidth, imageHeight, 1 );
}
//---------------------------------------------------------------------//
void TexturePacker::freeTree( Rect *pTree )
{
	if( pTree == NULL ) return;

	freeTree( pTree->pChildren[ LEFT ] );
	freeTree( pTree->pChildren[ RIGHT ] );

	delete pTree;
	pTree = NULL;
}
//---------------------------------------------------------------------//
bool TexturePacker::insertImage( Rect* pTree, const Image *pImage )
{
	if( pTree->isContainedWithin( pImage ) )
	{
		if( pTree->isAssigned( ) )
		{
			if( insertImage( pTree->pChildren[ LEFT ], pImage ) )
			{
				return true;
			}
			else
			{
				return insertImage( pTree->pChildren[ RIGHT ], pImage );
			}

		}
		else
		{
			pTree->assignImage( pImage );
			return true;
		}
	}
	
	return false;
}
//---------------------------------------------------------------------//
void TexturePacker::sortImages( )
{
	if( m_Images.size( ) > 1 )
	{
		std::sort( m_Images.begin(), m_Images.end(), imageAreaComparer );
		//assert( imageAreaComparer(m_Images[0], m_Images[1]) );
	}	
}
//---------------------------------------------------------------------//
bool TexturePacker::imageAreaComparer( const Image &left, const Image &right )
{
	ilBindImage( left );
	ILint leftWidth  = ilGetInteger( IL_IMAGE_WIDTH );
	ILint leftHeight = ilGetInteger( IL_IMAGE_HEIGHT );

	ilBindImage( right );
	ILint rightWidth  = ilGetInteger( IL_IMAGE_WIDTH );
	ILint rightHeight = ilGetInteger( IL_IMAGE_HEIGHT );

	return leftWidth * leftHeight > rightWidth * rightHeight;
}
//---------------------------------------------------------------------//
bool TexturePacker::Rect::isAssigned( ) const
{
	return pImage != NULL;
}
//---------------------------------------------------------------------//
bool TexturePacker::Rect::isContainedWithin( const Image *pImage ) const
{
	assert( pImage != NULL );
	ilBindImage( *pImage );
	uint imageWidth  = ilGetInteger( IL_IMAGE_WIDTH );
	uint imageHeight = ilGetInteger( IL_IMAGE_HEIGHT );
	return imageWidth <= width && imageHeight <= height;
}
//---------------------------------------------------------------------//
void TexturePacker::Rect::assignImage( const Image *pImage )
{
	assert( pImage != NULL );
	ilBindImage( *pImage );
	uint imageWidth  = ilGetInteger( IL_IMAGE_WIDTH );
	uint imageHeight = ilGetInteger( IL_IMAGE_HEIGHT );

	// Form a sub-rectangle along the longest edge.
	if( imageWidth < imageHeight ) // form rect along height-side
	{
		pChildren[ LEFT ]  = new Rect( x + imageWidth, y, width - imageWidth, imageHeight ); 
		pChildren[ RIGHT ] = new Rect( x, y + imageHeight, width, height - imageHeight ); 

		//assert( pChildren[ LEFT ]->area() <= pChildren[ RIGHT ]->area() );
	}
	else // imageWidth >= imageHeight
	{
		pChildren[ LEFT ]  = new Rect( x, y + imageHeight, imageWidth, height - imageHeight ); 
		pChildren[ RIGHT ] = new Rect( x + imageWidth, y, width - imageWidth, height ); 

		//assert( pChildren[ RIGHT ]->area() <= pChildren[ LEFT ]->area() );
	}

	this->pImage = pImage;
}
//---------------------------------------------------------------------//
