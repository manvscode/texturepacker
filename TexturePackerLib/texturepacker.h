#ifndef _TEXTUREPACKER_H_
#define _TEXTUREPACKER_H_

#include <vector>
#include <IL/il.h>

// This depends on the DevIL library.
#pragma comment(lib, "devil.lib")

typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  byte;
#ifdef UNICODE
typedef wchar_t        tchar;
#ifndef TEXT
#define TEXT(str)      (L##str)
#endif
#else
typedef char           tchar;
#ifndef TEXT 
#define TEXT(str)
#endif
#endif

//---------------------------------------------------------------------//
//                         Texture Packer                              //
//---------------------------------------------------------------------//
class TexturePacker 
{
  public:
	struct Image {
		ushort x;
		ushort y;
		ushort width;
		ushort height;
		ILuint imageId;

		byte *pixels( );
	};
	typedef std::vector<Image> ImageCollection;

  protected:
	enum { LEFT = 0, RIGHT = 1 };

	struct Rect {
		Rect( ushort x_, ushort y_, ushort w, ushort h )
			: x(x_), y(y_), width(w), height(h), pImage(NULL)
		{
			pChildren[ LEFT ]  = NULL;
			pChildren[ RIGHT ] = NULL;
		}

		ushort x;
		ushort y;
		ushort width;
		ushort height;
		Image* pImage;
		Rect*  pChildren[ 2 ];

		bool isAssigned( ) const;	
		bool isContainedWithin( const Image *pImage ) const;
		void assignImage( Image *pImage );
		uint area( ) const { return width * height; }
	};

	ImageCollection m_Images;
	Rect*           m_pRoot;
	ILuint          m_FinalImage;
	uint            m_FinalImageWidth;
	uint            m_FinalImageHeight;
	uint            m_FinalImageBytesPerPixel;

	bool blitTree( Rect *pTree, bool blend = true );
	void freeTree( Rect *&pTree );
	bool insertImage( Rect *&pTree, Image *pImage );
	void sortImages( );
	static bool imageAreaComparer( const Image &a, const Image &b );

  public:
	TexturePacker( );
	~TexturePacker( );

	int add( const tchar *imageFilename );
	void clear( );

	bool texture( uint width, uint height, ushort bytesPerPixel = 4 );
	byte *pixels( );
	bool save( const tchar *filename );

	const ImageCollection &images( ) const;
};

#endif // _TEXTUREPACKER_H_
